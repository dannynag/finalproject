package Utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.Screen;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class base 
{
	public static WebDriver driver;
	public static ExtentReports extent;
	public static Screen screen;
	public static ExtentTest test;
	public static String timeStamp = new SimpleDateFormat ("yyyy-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());
	/*public static PageObject.loginPage lp;
	public static PageObject.mainPage mp;
	
	public static void loginfunc()
	{
		lp = PageFactory.initElements(driver, PageObject.loginPage.class);
		mp = PageFactory.initElements(driver, PageObject.mainPage.class);
	}
	*/
	
	public static String getData (String nodeName) throws ParserConfigurationException, SAXException, IOException
	{
	File fXmlFile = new File("C:/Users/Danny/Desktop/ExternalFinalProj.xml");
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile); 
	doc.getDocumentElement().normalize();
	System.out.println(doc.getElementsByTagName(nodeName).item(0).getTextContent());
	return doc.getElementsByTagName(nodeName).item(0).getTextContent();

}
	
	public static void initBrowser (String browserType) throws ParserConfigurationException, SAXException, IOException
	{
		switch(browserType.toLowerCase())
		{
			case "chrome":
				driver = initChromeDriver();
				break;
			case "firefox":
				driver = initFirefoxDriver();
				break;
			case "IE":
				driver = initIEDriver();
				break;
		}
				driver.manage().window().maximize();
				driver.get(getData("URL"));
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				screen = new Screen();
		}
		
		public static WebDriver initFirefoxDriver() throws ParserConfigurationException, SAXException, IOException
		{
			System.setProperty("webdriver.gecko.driver", getData("FirefoxDrivePath"));
			WebDriver driverff = new FirefoxDriver();
			return driverff;
		}
		
		public static WebDriver initChromeDriver() throws ParserConfigurationException, SAXException, IOException
		{
			System.setProperty("webdriver.chrome.driver", getData("ChromeDrivePath"));
			WebDriver driverch = new ChromeDriver();
			return driverch;
		}
		
		public static WebDriver initIEDriver() throws ParserConfigurationException, SAXException, IOException
		{
			System.setProperty("webdriver.ie.driver", getData("IEDrivePath"));
			WebDriver driverie = new InternetExplorerDriver();
			return driverie;
		}
		
		public static void InstanceReport() throws ParserConfigurationException, SAXException, IOException
		{
			extent = new ExtentReports(getData("ReportFilePath") + getData ("ReportFileName")+ timeStamp + ".html",true);
				
		}
		
		public static void ExtentTest (String testname, String testdescription)
		{
			test = extent.startTest(testname, testdescription);
		}
		
		public static void FinaliseReportTest()
		{
			extent.endTest(test);
		}
		
		public static void FinaliseExtentReport()
		{
			extent.flush();
			extent.close();
		}
		
		public String screenshot() throws IOException, ParserConfigurationException, SAXException 
		{
			String SSpath  = getData("ReportFilePath") + "screenshot_" +  getrandomnumber() + ".png";
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(SSpath));
			return SSpath;
		}
		
		public int getrandomnumber ()
		{
			Random rand = new Random();
			int n = rand.nextInt(999999) + 1000;
			return n;
		}
		
		/*public void login()
		{
			mp.login.click();
			lp.user_name.sendKeys("Fitnessme");
			lp.user_password.sendKeys("Fitnessme");
			lp.login_button.click();
		}*/
	}




package Utilities;

import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;

import com.relevantcodes.extentreports.LogStatus;

import PageObject.loginPage;
import PageObject.mainPage;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

public class commonOps extends base
{
//public static mainPage mp = new mainPage();
//public static loginPage lp = new loginPage();

		 
	


	public void verifyElementExists (WebElement elem) throws IOException, ParserConfigurationException, SAXException
	{
		try
		{
			elem.isDisplayed();
			System.out.println("Element Displayed");
			test.log(LogStatus.PASS,"Element Displayed" );
		}
		catch (Exception e)
		{
			System.out.println("Element Not Displayed");
			test.log(LogStatus.FAIL, "Element Not Displayed" + test.addScreenCapture(screenshot()));
			fail("Element Not Displayed");
		}
	}
	public void verifyValueExists (WebElement elem, String expectedValue) throws IOException, ParserConfigurationException, SAXException
	{
		try
		{
		String actual = elem.getText();
		assertEquals(expectedValue,actual);
		System.out.println("Value Exists");
		test.log(LogStatus.PASS, "Value Exists");
	}
		catch (Exception e)
		{
			System.out.println("Value does not exist" + e.getMessage());
			test.log(LogStatus.FAIL, "Value does not Exist" + test.addScreenCapture(screenshot()));
			fail("Value does not Exists");
		}
		catch (AssertionError ae)
		{
			System.out.println("Assert Failed" + ae.getMessage());
			test.log(LogStatus.FAIL, "Assert Failed" + test.addScreenCapture(screenshot()));
			fail("Assert Failed");
		}
	}
	
	public void verifyImageExists (String image) throws IOException, ParserConfigurationException, SAXException
	{
		try
		{
			screen.find(getData("imagepath")+ image);
			System.out.println("Image Displayed");
			test.log(LogStatus.PASS,"Image Displayed" );
		}
		catch (Exception e)
		{
			System.out.println("Image Not Displayed");
			test.log(LogStatus.FAIL, "Image Not Displayed" + test.addScreenCapture(screenshot()));
			fail("Image Not Displayed");
		}
	}

		/*public void login()
		{
			mp.login.click();
			lp.user_name.sendKeys("Fitnessme");
			lp.user_password.sendKeys("Fitnessme");
			lp.login_button.click();
		}*/


}





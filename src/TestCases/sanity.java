package TestCases;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.xml.sax.SAXException;

import Utilities.base ;
import Utilities.commonOps;

public class sanity extends base 
{
	//static WebDriver driver;
	static PageObject.loginPage lp;
	static PageObject.mainPage mp;
	static PageObject.newSchedulePage nsp;
	static commonOps cmps = new commonOps();
	
	@BeforeClass
	public static void startsession() throws ParserConfigurationException, SAXException, IOException
	{
		initBrowser(getData("BrowserType"));
		InstanceReport();
		lp = PageFactory.initElements(driver, PageObject.loginPage.class);
		mp = PageFactory.initElements(driver, PageObject.mainPage.class);
		nsp = PageFactory.initElements(driver, PageObject.newSchedulePage.class);
		
		
	}
	
	@AfterClass
	public static void closesession()
	{
		FinaliseExtentReport();
		driver.quit();
	}
	
	@After
	public void aftertest()
	{
		FinaliseReportTest();
	}
	
	
	@Test
	public void test() throws IOException, ParserConfigurationException, SAXException
	{
		ExtentTest("test", "verify: Correct Error message exists");
		cmps.verifyImageExists("Supersaaslogo.png");
		//mp.login.click();
		//lp.user_name.sendKeys("Fitnessme"); 
		//lp.user_password.sendKeys("Fitnessme");
		//lp.login_button.click();
		lp.login();
		lp.new_schedule.click();
		nsp.resource_schedule.click();
		nsp.next_step1.click();
		nsp.next_step2.click();
		cmps.verifyElementExists(nsp.error_message);
	}
}


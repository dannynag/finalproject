package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utilities.base;

public class newSchedulePage extends base
{
	public WebDriver driver;
	
	@FindBy (how = How.ID, using ="type_resource")
	public WebElement resource_schedule;
	
	@FindBy (how = How.ID, using ="type_capacity")
	public WebElement capacity_schedule;
	
	@FindBy (how = How.ID, using ="type_service")
	public WebElement service_schedule;
	
	@FindBy (how = How.NAME, using ="next")
	public WebElement next_step1;
	
	@FindBy (how = How.ID, using ="type_booking_system")
	public WebElement booking_system;
	
	@FindBy (how = How.ID, using ="type_self_service")
	public WebElement selfservice_schedule;
	
	@FindBy (how = How.ID, using ="type_event_calendar")
	public WebElement event_calender;
	
	@FindBy (how = How.ID, using ="flash")
	public WebElement error_message;
	
	@FindBy (how = How.NAME, using ="next")
	public WebElement next_step2;
	
	//@FindBy (how = How.ID, using ="quantity_one")
	//public WebElement quantity_one;
	
	//@FindBy (how = How.ID, using ="quantity_many")
	//public WebElement quantity_many;
	
	//@FindBy (how = How.NAME, using ="next")
	//public WebElement next_step3;
	
	public newSchedulePage (WebDriver driver)
	{
	this.driver = driver;
	}
}

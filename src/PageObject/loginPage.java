package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utilities.base;

public class loginPage extends base  
{
	public WebDriver driver;
	
	@FindBy (how = How.ID, using = "so")
	public WebElement login;
	
	@FindBy (how = How.ID, using = "name")
	public WebElement user_name;
	
	@FindBy (how = How.ID, using = "password")
	public WebElement user_password;
	
	@FindBy (how = How.ID, using = "submit")
	public WebElement login_button;
	
	@FindBy (how = How.CSS, using = "a[href='/super_schedule/wizard']")
	public WebElement new_schedule;
			
	public loginPage (WebDriver driver)
	{
	this.driver = driver;
	}

	public void login() {
		{
			login.click();
			user_name.sendKeys("Fitnessme");
			user_password.sendKeys("Fitnessme");
			login_button.click ();
			//klklklk
		}
	}
}
